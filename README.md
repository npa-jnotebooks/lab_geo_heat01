# LAB_GEO_HEAT01

Jupyter Notebook per studiare la propagazione del calore per conduzione

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/npa-jnotebooks%2Flab_geo_heat01/master?filepath=UNIMIB_GEO_HEAT01_conductive_transfer.ipynb)
